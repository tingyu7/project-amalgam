# Project Amalgam

Putting together an all-in-one project that showcases some of the embedded software knowledge and techniques that I have acquired from classrooms and previous work experiences.

Details on Project Amalgam can be found here [Link](https://docs.google.com/document/d/13K3yeCzsxTxadv1jdjtT-GRe-Cr_P333t12WVlM3H0U/edit?usp=sharing)

This repo contains the Vehicle Supervisory Controller(VSC) portion of the Project Amalgam